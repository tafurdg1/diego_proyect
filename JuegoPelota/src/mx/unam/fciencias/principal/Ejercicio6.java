/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.unam.fciencias.principal;

import java.awt.Color;
import mx.unam.fciencias.core.Pelota;
import mx.unam.fciencias.core.Vector;
import mx.unam.fciencias.gui.VentanaPelota;

/**
 *
 * Objetivo:Visualizar diferentes pelotas rebotando entre si 
 * Descripcion: Visualización de multiples pelotas rebotando
 * 
 */
public class Ejercicio6 {

    public static void main(String[] args) {
        Pelota pelota1 = new Pelota(20, 10, 30, Color.CYAN);
        pelota1.setVelocidad(new Vector(4, 3));
        Pelota pelota2 = new Pelota(40, 20, 20, Color.RED);
        pelota2.setVelocidad(new Vector(2, 6));
        Pelota pelota3 = new Pelota(60, 30, 40, Color.BLUE);
        pelota3.setVelocidad(new Vector(8, 4));

        VentanaPelota q = new VentanaPelota();
        q.getPanelPelota().agregar(pelota1);
        q.getPanelPelota().agregar(pelota2);
        q.getPanelPelota().agregar(pelota3);
    }
}

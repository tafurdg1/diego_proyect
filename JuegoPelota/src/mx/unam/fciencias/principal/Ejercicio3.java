/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.unam.fciencias.principal;

import java.awt.Color;
import mx.unam.fciencias.core.Pelota;
import mx.unam.fciencias.core.Vector;
import mx.unam.fciencias.gui.VentanaPelota;

/**
 *
 * Objetivo:Visualizar pelota en movimiento vertical sin colisión con bordes
 * Descripcion: Visualización de pelota rebotando verticalmente
 */
public class Ejercicio3 {

    public static void main(String[] args) {
        Pelota pelota = new Pelota(16, 16, 15, Color.cyan);

        pelota.setVelocidad(new Vector(0, 2));
        VentanaPelota q = new VentanaPelota();
        q.getPanelPelota().agregar(pelota);

    }
}

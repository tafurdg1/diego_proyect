/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.unam.fciencias.principal;

import java.awt.Color;
import mx.unam.fciencias.core.Pelota;
import mx.unam.fciencias.gui.VentanaPelota;

/**
 *
 * Objetivo: Solucionar problema planteado por constructor copia
 * Descripcion:Visualizacion de dos diferentes pelotas
 */
public class Ejercicio2 {

    public Color c;

    public static void main(String[] args) {
        //Problema
//        Pelota pelota = new Pelota(100, 50, 20);
//        Pelota otraPelota = pelota;
//        otraPelota.getUbicacion().setY(200);
//        otraPelota.setColor(Color.MAGENTA);
//        Se observa que otrapelota esta tomando la misma instancia de pelota.
//        Se arregla llamando un contructor diferente para otrapelota.

        //Solucion
        VentanaPelota q = new VentanaPelota();
        Pelota pelota = new Pelota(100, 50, 20);
        Pelota otraPelota = new Pelota();
        otraPelota.getUbicacion().setY(200);
        otraPelota.setColor(Color.MAGENTA);
        q.getPanelPelota().agregar(pelota);
        q.getPanelPelota().agregar(otraPelota);
    }
}

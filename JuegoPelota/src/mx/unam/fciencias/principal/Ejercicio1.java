/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.unam.fciencias.principal;

import java.awt.Color;
import mx.unam.fciencias.core.Pelota;
import mx.unam.fciencias.core.Vector;
import mx.unam.fciencias.gui.VentanaPelota;

/**
 *
 * Objetivo: Probar los diferentes constructores de la clase Pelota
 * Descripcion: Visualizacion de diferentes tipos de pelotas
 */
public class Ejercicio1 {

    private static Color c;

    public static void main(String[] args) {
        VentanaPelota q = new VentanaPelota();

        Pelota pelota1 = new Pelota();
        Pelota pelota2 = new Pelota(100, 150, 15);
        Pelota pelota3 = new Pelota(100, 70, 50, c.BLUE);
        Pelota pelota4 = new Pelota(pelota1);
        Vector ubicacion = new Vector(pelota4.getUbicacion().getX() + 10, pelota4.getUbicacion().getY());
        pelota4.setUbicacion(ubicacion);
        pelota4.setColor(c.RED);

        q.getPanelPelota().agregar(pelota1);
        q.getPanelPelota().agregar(pelota2);
        q.getPanelPelota().agregar(pelota3);
        q.getPanelPelota().agregar(pelota4);
    }
}

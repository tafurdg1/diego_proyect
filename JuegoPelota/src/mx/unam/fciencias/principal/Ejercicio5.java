package mx.unam.fciencias.principal;

import java.awt.Color;
import mx.unam.fciencias.core.Pelota;
import mx.unam.fciencias.core.Vector;
import mx.unam.fciencias.gui.VentanaPelota;

/**
 *
 * Objetivo: Visualizar pelota botando en el area 
 * Descripcion: Visualización de pelota rebotando totalmente
 */
public class Ejercicio5 {

    public static void main(String[] args) {
        Pelota pelota = new Pelota(-1, 1025, 101, Color.GREEN);
        pelota.setVelocidad(new Vector(2,3));
        
        VentanaPelota q = new VentanaPelota();
        q.getPanelPelota().agregar(pelota);
    }
}

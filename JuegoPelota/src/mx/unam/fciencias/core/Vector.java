package mx.unam.fciencias.core;

/**
 *
 * Clase Vector con sus diferentes variables, constructores y métodos
 *
 */
public class Vector {

    private int x;
    private int y;

    public Vector() {
    }

    public Vector(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public Vector(Vector vector) {

    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    //Metodo que suma vectores
    public Vector sumar(Vector otroVector) {

        Vector vectorNuevo = new Vector();
        vectorNuevo.setX(x + otroVector.getX());
        vectorNuevo.setY(y + otroVector.getY());

        return vectorNuevo;
    }

    @Override
    public String toString() {
        return "Vector{" + "x=" + x + ", y=" + y + '}';
    }

}

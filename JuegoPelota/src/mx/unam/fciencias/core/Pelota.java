package mx.unam.fciencias.core;

import java.awt.Color;
import javax.swing.JOptionPane;

/**
 *
 * Clase Pelota con sus diferentes variables, constructores y métodos
 *
 */
public class Pelota {

    private Vector ubicacion;
    private Vector velocidad;
    private int radio;
    private Color color;

    public Pelota() {
        radio = 20;
        ubicacion = new Vector(25, 25);
        velocidad = new Vector(0, 0);
        color = color.BLACK;
    }

    public Pelota(int x, int y, int radio) {
        // Validación de componentes
        if (x < 0 || x > 1024) {
            JOptionPane.showMessageDialog(null, "El valor de x debe estar entre"
                    + "0 y 1024, por lo tanto se deja el valor por defecto");
            x = 25;
        }
        if (y < 0 || y > 1024) {
            JOptionPane.showMessageDialog(null, "El valor de y es "
                    + "incorrecto, se le asigna el valor por defecto");
            y = 25;
        }
        // Validación del radio
        if (radio < 5 || radio > 100) {
            JOptionPane.showMessageDialog(null, "El valor del radio es "
                    + "incorrecto, se le asigna el valor por defecto");
            this.radio = 20;
        } else {
            this.radio = radio;
        }
        ubicacion = new Vector(x, y);
        velocidad = new Vector(0, 0);
        color = color.BLACK;
    }

    public Pelota(int x, int y, int radio, Color color) {
        // Validación de componentes
        if (x < 0 || x > 1024) {
            JOptionPane.showMessageDialog(null, "El valor de x debe estar entre"
                    + "0 y 1024, por lo tanto se deja el valor por defecto");
            x = 25;
        }
        if (y < 0 || y > 1024) {
            JOptionPane.showMessageDialog(null, "El valor de y es "
                    + "incorrecto, se le asigna el valor por defecto");
            y = 25;
        }
        // Validación del radio
        if (radio < 5 || radio > 100) {
            JOptionPane.showMessageDialog(null, "El valor del radio es "
                    + "incorrecto, se le asigna el valor por defecto");
            this.radio = 20;
        } else {
            this.radio = radio;
        }
        ubicacion = new Vector(x, y);
        velocidad = new Vector(0, 0);
        this.color = color;
    }

    public Pelota(Pelota otraPelota) {
        radio = otraPelota.getRadio();
        ubicacion = otraPelota.getUbicacion();
        velocidad = otraPelota.getVelocidad();
        color = otraPelota.getColor();
    }

    public void botar() {
        //Mensaje que imprime la consola
        System.out.println("Botando desde [" + ubicacion.getX() + " , "
                + ubicacion.getY() + "]");

    }

    public void botar(Boolean verticalmente) {

        int xN = -velocidad.getX();
        int yN = -velocidad.getY();

        // Validación vertical
        if (verticalmente) {
            velocidad.setY(yN);
        } else {
            velocidad.setX(xN);
        }
        botar();
    }

    public void mover() {
        ubicacion = ubicacion.sumar(velocidad);

    }

    public Vector getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Vector ubicacion) {
        this.ubicacion = ubicacion;
    }

    public Vector getVelocidad() {
        return velocidad;
    }

    public void setVelocidad(Vector velocidad) {
        this.velocidad = velocidad;
    }

    public int getRadio() {
        return radio;
    }

    public void setRadio(int radio) {
        this.radio = radio;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Pelota{" + "ubicacion=" + ubicacion + ", velocidad=" + velocidad + ", radio=" + radio + ", color=" + color + '}';
    }

}

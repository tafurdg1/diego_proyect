package mx.unam.fciencias.core;

import mx.unam.fciencias.gui.PanelPelota;

/**
 * Clase de objetos que verifican las colisiones que se dan con el entorno
 */
public class VerificadorColisiones {

    PanelPelota panelPelota;

    public VerificadorColisiones(PanelPelota panel) {
        panelPelota = panel;
    }

    public void verificarColision(Pelota pelota) {
        // Verificación de la colisión con los bordes superior e inferior
        int ubicacionX = pelota.getUbicacion().getX();
        int ubicaciony = pelota.getUbicacion().getY();

        if (ubicaciony < 0 || ubicaciony > 450) {
            pelota.botar(true);
        }
        // Verificación con los bordes izquierdo y derecho
        if (ubicacionX < 0 || ubicacionX > 450) {
            pelota.botar(false);
        }
    }
}

package mx.unam.fciencias.gui;


import mx.unam.fciencias.core.Pelota;
import mx.unam.fciencias.core.VerificadorColisiones;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.Timer;

/**
 * Panel donde se dibujarán las pelotas añadidas
 */
public class PanelPelota extends JPanel {

    private Timer timer;
    private List<Pelota> pelotas;
    private VerificadorColisiones verificador;
    
    public PanelPelota() {
        setBackground(Color.WHITE);
        
        pelotas = new ArrayList<>();
        verificador = new VerificadorColisiones(this);
        
        timer = new Timer(1000/60, new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                repaint();
            }
        });
        
        timer.start();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        
        // Se pintan todas las pelotas de la lista
        for (Pelota pelota : pelotas) {
            g.setColor(pelota.getColor());
            pintarPelota(g, pelota);
            pelota.mover();

            // Se verifica si la pelota colisiona con los bordes
            verificador.verificarColision(pelota);
        }
        
    }

    /**
     * Agrega una pelota a la lista
     * @param nuevaPelota 
     */
    public void agregar(Pelota nuevaPelota) {
        if(nuevaPelota != null)
            pelotas.add(nuevaPelota);
    }
    
    /**
     * Pinta la pelota en el panel
     * @param g
     * @param pelota 
     */
    private void pintarPelota(Graphics g, Pelota pelota) {
        g.fillOval(pelota.getUbicacion().getX()-pelota.getRadio(), pelota.getUbicacion().getY()-pelota.getRadio(), 
                pelota.getRadio()*2, pelota.getRadio()*2);
    }
}

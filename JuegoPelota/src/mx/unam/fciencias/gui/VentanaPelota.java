package mx.unam.fciencias.gui;


import javax.swing.JFrame;

/**
 * Ventana donde se insertará el panel
 */
public class VentanaPelota extends JFrame{
    private PanelPelota panelPelota;
    
    public VentanaPelota(){
        super("Pelota");
        
        inicializarComponentes();
        
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(500,500);
        setAlwaysOnTop(true);
        setVisible(true);
    }
    
    private void inicializarComponentes(){
        panelPelota = new PanelPelota();
        
        add(panelPelota);
        
    }

    public PanelPelota getPanelPelota() {
        return panelPelota;
    }
    
    
}
